# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# SPDX-FileCopyrightText: 2022, 2024 Wantoyèk <wantoyek@gmail.com>
# Linerly <linerly@protonmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-19 00:40+0000\n"
"PO-Revision-Date: 2024-03-03 11:47+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: https://t.me/Localizations_KDE_Indonesia\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Default"
msgstr ""

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Potret"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Lanskap"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Potret Terbalik"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Lanskap Terbalik"

#: kcmtablet.cpp:86
#, fuzzy, kde-format
#| msgid "Follow the active screen"
msgid "Follow the Current Screen"
msgstr "Ikuti layar yang aktif"

#: kcmtablet.cpp:91
#, kde-format
msgid "All Screens"
msgstr ""

#: kcmtablet.cpp:100
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:160
#, kde-format
msgid "Fit to Screen"
msgstr ""

#: kcmtablet.cpp:161
#, kde-format
msgid "Keep Aspect Ratio and Fit Within Screen"
msgstr ""

#: kcmtablet.cpp:162
#, kde-format
msgid "Map to Portion of Screen"
msgstr ""

#: ui/main.qml:31
#, kde-format
msgid "No drawing tablets found"
msgstr "Tidak ada tablet penggambar yang ditemukan"

#: ui/main.qml:32
#, kde-format
msgid "Connect a drawing tablet"
msgstr "Hubungkan sebuah tablet penggambar"

#: ui/main.qml:40
#, fuzzy, kde-format
#| msgid "Tablet"
msgctxt "Tests tablet functionality like the pen"
msgid "Test Tablet…"
msgstr "Tablet"

#: ui/main.qml:66
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Peranti:"

#: ui/main.qml:104
#, kde-format
msgid "Map to screen:"
msgstr ""

#: ui/main.qml:124
#, kde-format
msgid "Orientation:"
msgstr "Orientasi:"

#: ui/main.qml:136
#, fuzzy, kde-format
#| msgid "Left-handed mode:"
msgid "Left-handed mode:"
msgstr "Mode tangan kiri:"

#: ui/main.qml:147
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Tells the device to accommodate left-handed users. Effects will vary by "
"device, but often it reverses the pad buttonsʼ functionality so the tablet "
"can be used upside-down."
msgstr ""

#: ui/main.qml:153
#, kde-format
msgid "Mapped Area:"
msgstr ""

#: ui/main.qml:240
#, kde-format
msgid "Resize the tablet area"
msgstr "Ubah ukuran area tablet"

#: ui/main.qml:264
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Kunci rasio aspek"

#: ui/main.qml:272
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: ui/main.qml:281
#, fuzzy, kde-format
#| msgid "Pen Button 1"
msgid "Pen button 1:"
msgstr "Tombol Pena 1"

#: ui/main.qml:282
#, fuzzy, kde-format
#| msgid "Pen Button 2"
msgid "Pen button 2:"
msgstr "Tombol Pena 2"

#: ui/main.qml:283
#, fuzzy, kde-format
#| msgid "Pen Button 3"
msgid "Pen button 3:"
msgstr "Tombol Pena 3"

#: ui/main.qml:327
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Papan:"

#: ui/main.qml:338
#, kde-format
msgid "None"
msgstr "Tidak ada"

#: ui/main.qml:360
#, fuzzy, kde-format
#| msgid "Button %1:"
msgid "Pad button %1:"
msgstr "Tombol %1:"

#: ui/Tester.qml:25
#, fuzzy, kde-format
#| msgid "Tablet"
msgctxt "@title"
msgid "Tablet Tester"
msgstr "Tablet"

#: ui/Tester.qml:48
#, kde-format
msgid "Stylus press X=%1 Y=%2"
msgstr ""

#: ui/Tester.qml:58
#, kde-format
msgid "Stylus release X=%1 Y=%2"
msgstr ""

#: ui/Tester.qml:68
#, kde-format
msgid "Stylus move X=%1 Y=%2"
msgstr ""

#: ui/Tester.qml:194
#, kde-format
msgid ""
"## Legend:\n"
"# X, Y - event coordinate\n"
msgstr ""

#: ui/Tester.qml:201
#, kde-format
msgctxt "Clear the event log"
msgid "Clear"
msgstr ""

#~ msgid "Primary (default)"
#~ msgstr "Utama (bawaan)"

#~ msgid "Fit to Output"
#~ msgstr "Pas ke Keluaran"

#~ msgid "Fit Output in tablet"
#~ msgstr "Pas Keluaran dalam tablet"

#~ msgid "Custom size"
#~ msgstr "Ukuran kustom"

#~ msgid "Target display:"
#~ msgstr "Target layar:"

#~ msgid "Area:"
#~ msgstr "Area:"

#~ msgid "Tool Button 1"
#~ msgstr "Tombol Alat 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Wantoyèk"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wantoyek@gmail.com"
